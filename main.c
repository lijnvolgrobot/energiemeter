#include <msp430.h> 
#include <stdint.h>
#include <string.h>
#include "lib/gpio.h"
#include "lib/i2c.h"
#include "lib/oled.h"
#include "lib/fonts.h"
#include "lib/temperatuur.h"
void scherm();
int interupt_counter;
float aantal_seconde = 0;
float ADC_Spanning = 0;
float ADC_Stroom = 0;
float pin_toggle = 0;
float spanning_ingang = 0;
float spanning_uitgang = 0;
float stroom = 0;
float joule = 0;
float R_Load = 0;
float ADC_SPANNING;
int stroom_deci = 0;
int stroom_gehele = 0;
int spanning_deci = 0;
int spanning_gehele = 0;
int stroom_deci_eerst = 0;
int stroom_gehele_eerst = 0;

//stelt adc settings in
void ADC_settings(void)
{
        ADC10CTL1 = INCH_5 + ADC10DIV_7 ;
        ADC10CTL0 = SREF_1 +  + REFBURST + REFON + REF2_5V + ADC10ON + ADC10IE + ENC ;
}
//stelt timer settings in en bepaald de interrupts per seconde
void timer0_settings(void)
{
    TA1CTL = TASSEL_2 + ID_3 + MC_1 + TAIE;
    TA1CCR0 = 50000;

}
//het protocol wat uitgevoerd wanneer een interupt word getriggered.
//dit protocol leest de pins uit.
//ook berekend het een aantal waardes
#pragma vector = ADC10_VECTOR
__interrupt void ADC(void)
{
    if (pin_toggle == 0) {
        //koppelt de ADC10MEM aan een variable zodat hiermee gerekend kan worden
        ADC_Stroom = ADC10MEM;
        stroom_deci_eerst = ((ADC_Stroom * 0.00244140625 * 2.0) - stroom_gehele_eerst) * 1000;
        stroom_gehele_eerst = (ADC_Stroom * 0.00244140625 * 2.0 * 1000) / 1000;
        //kleine correctie op de stroom onder de 100 mA.
        if ((stroom_deci_eerst < 100) & (stroom_gehele_eerst < 1))
        {
            stroom = ADC_Stroom * 0.00244140625 * 2.0 * 1.35;
        }
        else {
            stroom = ADC_Stroom * 0.00244140625 * 2.0;
        }
        joule = aantal_seconde * (stroom) * (spanning_uitgang) ;
        //zorgt voor de juiste decimale weergave van de stroom
        stroom_gehele = (stroom * 1000) / 1000;
        stroom_deci = (stroom - stroom_gehele) * 1000;
        //dissabled de enc zodat de pin naar 1.4 verandert kan worden en enabled hem daarna weer. 
        pin_toggle = 1;
        ADC10CTL0 &= ~ENC;
        ADC10CTL1 = INCH_4 + ADC10DIV_7;
        ADC10CTL0 |= ENC;

    }
    else if (pin_toggle == 1) 
    {
        ADC_Spanning = ADC10MEM;
        spanning_ingang = ADC_Spanning * 0.00244140625 * 7.2;
        spanning_uitgang = spanning_ingang - (stroom * 0.1);
        R_Load = (spanning_uitgang / stroom) * 10.0;
        spanning_gehele = (spanning_uitgang * 100) / 100;
        spanning_deci = (spanning_uitgang - spanning_gehele) * 100;
        pin_toggle = 0;
        ADC10CTL0 &= ~ENC;
        ADC10CTL1 = INCH_5 + ADC10DIV_7;
        ADC10CTL0 |= ENC;
    }
// een timer die interupts genereerd en de tijd bij houdt
#pragma vector = TIMER1_A1_VECTOR
__interrupt void timer1(void)
{
    interupt_counter = interupt_counter + 1;
    if (interupt_counter == 40)
    {
        aantal_seconde = aantal_seconde + 1;
        interupt_counter = 0;
    }
    ADC10CTL0 |= ADC10SC;
    TA1CTL &= ~TAIFG;

}


#pragma vector = TIMER0_A0_VECTOR
__interrupt void tijd(void){
time_passed += 1;
scherm(afstand());
}



void main(void) {

    __enable_interrupt();

    WDTCTL = WDTPW | WDTHOLD;   // Stop watchdog timer
    TA0CCR0 = 1800;
    BCSCTL3 = LFXT1S_2;
    TA0CCTL0 = CCIE;
    TA0CTL = TASSEL_1 | ID_3 | MC_1;

    DCOCTL = 0;
    BCSCTL1 = CALBC1_16MHZ; // Set range
    DCOCTL = CALDCO_16MHZ;  // Set DCO step + modulation */


    ADC_settings();
    timer0_settings();

    //Zet display aan
    oledInitialize();
    //eventueel flippen
    oledSetOrientation(FLIPPED);
    //begin met leeg scherm
    oledClearScreen();


    while(1)
    {
        afstand();
    }

}

//zorgt voor weergave op het scherm. 
void scherm(int afst){
    oledClearScreen();
    char SpanningT[20];
    itoa(spanning_uitgang,SpanningT);
    char stroom_geheleT[20];
    itoa(stroom_gehele,stroom_geheleT);
    char stroom_deciT[20];
    itoa(stroom_deci,stroom_deciT);
    char spanning_geheleT[20];
    itoa(spanning_gehele,spanning_geheleT);
    char spanning_deciT[20];
    itoa(spanning_deci,spanning_deciT);
    char jouleT[20];
    itoa(joule,jouleT);
    char tijdT[20];
    itoa(aantal_seconde,tijdT);
    char RloadT[20];
    itoa(R_Load,RloadT);
    oledPrint(1,1,"Energiemeter",small);
    oledPrint(1,11,"Stroom:",small);
    oledPrint(50,11,stroom_geheleT,small);
    oledPrint(55,11,".",small);
    //zorgt er voor dat wanneer de stroom onder de 100 mA komt er drie decimale blijven staan voor correcte weergave
    if (stroom_deci < 100)
    {
        oledPrint(60,11,"0",small);
        oledPrint(65,11,stroom_deciT,small);
    }
    else {
        oledPrint(60,11,stroom_deciT,small);
    }
    oledPrint(80,11,"A",small);
    oledPrint(1,20,"Spanning:",small);
    oledPrint(50,20,spanning_geheleT,small);
    oledPrint(64,20,spanning_deciT,small);
    oledPrint(60,20,".",small);
    oledPrint(80,20,"V",small);
    oledPrint(1,30,"Joule:",small);
    oledPrint(30,30,jouleT,small);
    oledPrint(60,30,"J",small);
    oledPrint(70,30,RloadT,small);
    oledPrint(100,30,"Ohm",small);
    oledPrint(120,20,"S",small);
    oledPrint(100,20,tijdT,small);

}

